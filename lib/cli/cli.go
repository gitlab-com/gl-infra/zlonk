package cli

import (
  "fmt"
  "os"
)

func ExitOk(s ...string) {
  if len(s) > 0 {
    fmt.Fprintf(os.Stdout, "%s\n", s[0])
  }
  os.Exit(0)
}

func ExitFatal(s string) {
  fmt.Fprintf(os.Stderr, "error: %s\n", s)
  os.Exit(-1)
}
