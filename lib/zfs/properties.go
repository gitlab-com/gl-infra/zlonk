package zfs

type Property struct {
	Name        string
	Value       string
	Source      string
}
type Properties map[string]*Property
