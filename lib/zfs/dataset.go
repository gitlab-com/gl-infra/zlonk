package zfs

import (
	"bufio"
	"errors"
	"fmt"
	"os/exec"
	"strings"
)

type Dataset struct {
	Name        string
	Pool        string
	Path        string
	Type        string
	Mountpoint  string
	Properties  Properties
}

func NewDataset(name ...string) *Dataset {
	switch len(name) {
	case 0:
		return &Dataset{
			Properties: make(map[string]*Property),
		}
	case 1:
		s := strings.Split(name[0], "/")
		return &Dataset{
			Name:       name[0],
			Path:       name[0],
			Pool: s[0],
			Properties: make(map[string]*Property),
		}
	default:
		return nil
	}
}

func (dataset *Dataset) Create() (err error)  {
	_, err = dataset.execZFS("create")
	if err != nil {
		return err
	}

	err = dataset.Get()
	if err != nil {
		return err
	}
	return nil
}

func (dataset *Dataset) Snapshot(snapshot string) (err error)  {
	_, err = dataset.execZFS("snapshot", snapshot)
	if err != nil {
		return err
	}

	if err = dataset.Get(); err != nil {
		return err
	}
	return nil
}

func (dataset *Dataset) Clone(snapshot string) (err error)  {
	_, err = dataset.execZFS("clone", snapshot)
	if err != nil {
		return err
	}

	if err = dataset.Get(); err != nil {
		return err
	}
	return nil
}

func (dataset *Dataset) Find(path string) (err error) {
	var out []string
	var c int = 0                            // zfs list -H -o name,mountpoint: column 0 is name; column 1 is mountpoint

	if strings.HasPrefix(path, "/") {  // unlike file system paths, ZFS paths do not start with a slash.
		c = 1
	}

// TOOO fix mocks
	out, err = dataset.execZFS("/tmp/zfslist")
	if err != nil {
		return err
	}

	for _, line := range out {
		s := strings.Fields(line)
		if len(s) == 2 {
			if s[c] == path {
				dataset.Name = s[0]
				dataset.Path = s[0]
				dataset.Mountpoint = s[1]
				break
			}
		}
	}

	if dataset.Name == "" {
		return errors.New(fmt.Sprintf("no dataset found for mountpoint %s", path))
	}

	if err = dataset.Get(); err != nil {
		return err
	}

	return nil
}

func (dataset *Dataset) Get() (err error) {
	var out []string
// TOOO fix mocks
	out, err = dataset.execZFS("/tmp/zfsgetall")
	if err != nil {
		return err
	}

	for _, line := range out {
		s := strings.Fields(line)

		if len(s) >= 4 && dataset.Name == s[0] {
			dataset.Properties[s[1]] = &Property{
				Name:   s[1],
				Value:  s[2],
				Source: strings.Join(s[3:], " ")}
		} else {
			fmt.Printf("ignored %s\n", line)
		}
	}

	if vt, okt := dataset.Properties["type"]; okt {
		if _, okt := dataset.Properties["origin"]; okt {
			dataset.Type = "clone"
		} else {
			dataset.Type = vt.Value
		}
	}

	if vm, okm := dataset.Properties["mountpoint"]; okm {
		dataset.Mountpoint = vm.Value
	}

	return nil
}

func (dataset *Dataset) Set(properties ...Property) (err error) {
		for _, p := range properties {
			_, err = dataset.execZFS(fmt.Sprintf("%s=%s", p.Name, p.Value))
			if err != nil {
				return err
			}
		}
}

func (dataset *Dataset) execZFS(arg ...string) (output []string, err error) {
// TOOO fix mocks
	cmd := exec.Command("cat", arg...)

	pipe, _ := cmd.StdoutPipe()
	cmd.Stderr = cmd.Stdout

	done := make(chan struct{})

	scanner := bufio.NewScanner(pipe)

	go func() {
		for scanner.Scan() {
			line := scanner.Text()
			output = append(output, line)
		}
		done <- struct{}{}
	}()

	err = cmd.Start()

	if err != nil {
		return output, err
	}

	<-done

	err = cmd.Wait()
	if err != nil {
		return output, err
	}

	return output, nil
}

func (dataset *Dataset) exists() bool {
	return true
}
