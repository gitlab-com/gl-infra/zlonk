#!/bin/bash

# Second iteration of script to create cloned replicas. PG12 ONLY.
#
# Approach:
#
# This script is executed twice per run: before said run, and after.
#
# The "before" run will:
#
# - Create snapshot of current patroni replica data directory dataset
# - Create a clone of said snapshot (the cloned replica)
# - Copy postgres configuration files (which have hardcoded paths to the clone) to cloned replica
# - Start Postgres cloned replica, which will go into recovery mode
#
# The "after" run will:
#
# - Stop the clone replica Postgres instance
# - Destroy the clone replica and the Patroni snapshot
#
# The script will determine "before" or "after" based on the existence of the snapshot:
#
# - snapshot does not exit: "before"
# - snapshot exists: "after"
#

set -euf -o pipefail

# =============================================================================
ZFS_PG_DATASET_BASE='zpool0/pg_datasets'
# =============================================================================

PTH_BASE=$(/sbin/zfs get -H -o value com.gitlab.zlonk:path.data.base "${ZFS_PG_DATASET_BASE}")
LOG_BASE=$(/sbin/zfs get -H -o value com.gitlab.zlonk:path.log.base "${ZFS_PG_DATASET_BASE}")
PGD_NAME=$(/sbin/zfs get -H -o value com.gitlab.zlonk:pg.data "${ZFS_PG_DATASET_BASE}")

if [ "$#" -ne 2 ]; then
  echo "usage: ${0} <project_name> <instance_name>" >&2
  exit 1
fi

PROJECT=$1
INSTANCE=$2
shift 2

PTH_PG_LOG_BASE="${LOG_BASE}/${PROJECT}/${INSTANCE}"

if [ ! -w "${PTH_PG_LOG_BASE}" ]; then
  echo "error: invalid"
  exit 1
fi

SNAPNAME="${PROJECT}.${INSTANCE}"

ZFS_PAR_DATA_BASE="${ZFS_PG_DATASET_BASE}/${PGD_NAME}"
ZFS_PAR_DATA_SNAP="${ZFS_PAR_DATA_BASE}@${SNAPNAME}"

ZFS_CLR_DATA_BASE="${ZFS_PAR_DATA_BASE}:${SNAPNAME}"
PTH_CLR_DATA_BASE="${PTH_BASE}/${PGD_NAME}:${SNAPNAME}"
PTH_CLR_LOG_FILE="${PTH_PG_LOG_BASE}/postgresql.csv"
PTH_CLR_CONF_FILE="${PTH_CLR_DATA_BASE}/postgresql.${PROJECT}.${INSTANCE}.conf"

PTH_ZLONK_CONF_BASE="/var/opt/gitlab/postgresql/opt/zlonk/etc/postgresql/${PROJECT}"
PTH_ZLONK_CONF_FILE="${PTH_ZLONK_CONF_BASE}/postgresql.${PROJECT}.${INSTANCE}.conf"

# =============================================================================
# OBSERVABILITY

O11Y_PUSH_GATEWAY='blackbox-01-inf-gprd.c.gitlab-production.internal'
O11Y_TIER='db'
O11Y_TYPE='zlonk.postgres'
O11Y_RESOURCE="zlonk.${SNAPNAME}"
O11Y_JOB='unknown'

# =============================================================================
# FUNCTIONS
#
# All funcions take a catastrophic view of the procedure, i.e., any failure will lead to immediate terminartion
# through efatal().

function efatal {
  if [ "$#" -ne 2 ]; then
    echo "invalid call for $0: ${*}" >&2
    exit 1
  else
    rcode=$1
    shift
    echo "fatal: ${*}" >&2
    exit "$rcode"
  fi
}

function ewarn {
  if [ "$#" -ne 2 ]; then
    echo "invalid call for $0: ${*}" >&2
    return 1
  else
    rcode=$1
    shift
    echo "warning: ${*}" >&2
    return "$rcode"
  fi
}

function o11y_start {   # job timeout
  if [ "$#" -ne 2 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    o11y_job=$1
    o11y_max_age=$2
    shift 2
    echo " + info: o11y_start ${o11y_job}"
    cat <<PROM | curl -s -o /dev/null -w '   -> HTTP response: %{http_code}\n' -i --data-binary @- "http://${O11Y_PUSH_GATEWAY}:9091/metrics/job/${o11y_job}/tier/${O11Y_TIER}/type/${O11Y_TYPE}"
# HELP gitlab_job_start_timestamp_seconds The start time of the job.
# TYPE gitlab_job_start_timestamp_seconds gauge
gitlab_job_start_timestamp_seconds{resource="${O11Y_RESOURCE}"} $(date +%s)
# HELP gitlab_job_success_timestamp_seconds The time the job succeeded.
# TYPE gitlab_job_success_timestamp_seconds gauge
gitlab_job_success_timestamp_seconds{resource="${O11Y_RESOURCE}"} 0
# HELP gitlab_job_max_age_seconds How long the job is allowed to run before marking it failed.
# TYPE gitlab_job_max_age_seconds gauge
gitlab_job_max_age_seconds{resource="${O11Y_RESOURCE}"} ${o11y_max_age}
# HELP gitlab_job_failed Boolean status of the job.
# TYPE gitlab_job_failed gauge
gitlab_job_failed{resource="${O11Y_RESOURCE}"} 0
PROM
  fi
}

function o11y_sucess {   # job
if [ "$#" -ne 1 ]; then
  efatal 1 "$0: invalid number of arguments"
  return 1
else
  o11y_job=$1
  shift
  echo " + info: o11y_success ${o11y_job}"
  cat <<PROM | curl -s -o /dev/null -w '   -> HTTP response: %{http_code}\n' -i --data-binary @- "http://${O11Y_PUSH_GATEWAY}:9091/metrics/job/${o11y_job}/tier/${O11Y_TIER}/type/${O11Y_TYPE}"
# HELP gitlab_job_success_timestamp_seconds The time the job succeeded.
# TYPE gitlab_job_success_timestamp_seconds gauge
gitlab_job_success_timestamp_seconds{resource="${O11Y_RESOURCE}"} $(date +%s)
PROM
  fi
}

function o11y_failed { # job
  if [ "$#" -ne 1 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    o11y_job=$1
    shift
    echo " + info: o11y_failed ${o11y_job}"
    cat <<PROM | curl -s -o /dev/null -w '   -> HTTP response: %{http_code}\n' -i --data-binary @- "http://${O11Y_PUSH_GATEWAY}:9091/metrics/job/${o11y_job}/tier/${O11Y_TIER}/type/${O11Y_TYPE}"
# HELP gitlab_job_failed Boolean status of the job.
# TYPE gitlab_job_failed gauge
gitlab_job_failed{resource="${O11Y_RESOURCE}"} 1
PROM
  fi
}

function create_snapshot {
  if [ "$#" -ne 1 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    snapshot=$1
    shift
    if /sbin/zfs snapshot "${snapshot}"; then
      echo " + info: created snapshot ${snapshot}"
      return 0
    else
      efatal 1 "failed to create snapshot ${snapshot}"
    fi
  fi
}

function destroy_snapshot {
if [ "$#" -ne 1 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    snapshot=$1
    shift
    if /sbin/zfs destroy "${snapshot}"; then
      echo " + info: destroyed snapshot ${snapshot}"
      return 0
    else
      efatal 1 "failed to destroy snapshot ${snapshot}"
    fi
  fi
}

function create_clone {
  if [ "$#" -ne 3 ];then
    efatal 1 "invalid number of arguments"
    return 1
  else
    snapshot=$1
    clone=$2
    clone_path=$3
    shift 3
    if /sbin/zfs clone -o mountpoint="${clone_path}" "${snapshot}" "${clone}"; then
      echo " + info: created clone ${clone} from snapshot ${snapshot}"
      return 0
    else
      efatal 1 "failed to create clone ${clone} from ${snapshot}"
    fi
  fi
}

function destroy_clone {
  if [ "$#" -ne 1 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    clone=$1
    shift
    if /sbin/zfs destroy "${clone}"; then
      echo " + info: destroyed clone ${clone}"
      return 0
    else
      efatal 1 "failed to destroy clone ${clone}"
    fi
  fi
}

function checkpoint_postgres {
  echo "  + checkpoint patroni replica"
  if time su - gitlab-psql -c "gitlab-psql -c \"checkpoint;\""; then
    echo "    + done"
  else
    efatal 1 "failed to checkpoint patroni replica"
  fi
}

function create_index {
  if [ "$#" -ne 2 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    table=$1
    column=$2
    index="idx_${table}_${column}"
    echo "  + create index ${index}"
    if time su - gitlab-psql -c "gitlab-psql -p 25432 -c \"create index ${index} on ${table} using btree (${column});\""; then
      echo "    + done"
    else
      efatal 1 "failed to create index ${index}"
    fi
  fi
}

function configure_clone_replica {
  if [ "$#" -ne 2 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    conf_file=$1
    data_dir=$2
    shift 2

    if rm "${data_dir}/postmaster.pid" ; then # >/dev/null 2>&1; then
      echo "  + deleted postmaster files"
    else
      efatal 1 "failed to delete postmaster files in clone"
    fi

    if rm "${data_dir}/postgresql.conf" "${data_dir}/postgresql.base.conf" ; then # >/dev/null 2>&1; then
      echo "  + deleted configuration files"
    else
      efatal 1 "failed to delete configuration files in clone"
    fi

    if cp -p "${conf_file}" "${data_dir}/"; then
       echo "  + copied configuration file"
    else
       efatal 1 "failed to copy configuration files in clone"
    fi
  fi
}

function start_postgres {
  if [ "$#" -ne 2 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    data_dir=$1
    conf_file=$2
    shift 2

    su - gitlab-psql -c "/usr/lib/postgresql/12/bin/pg_ctl start -o \"-c config_file=${conf_file}\" -D ${data_dir}"
  fi
}

function stop_postgres {
  if [ "$#" -ne 3 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    data_dir=$1
    conf_file=$2
    log_file=$3

    su - gitlab-psql -c "/usr/lib/postgresql/12/bin/pg_ctl stop -m immediate -o \"-c config_file=${conf_file}\" -D ${data_dir}"
    cp /dev/null "${log_file}"
  fi
}

function is_postgres_ready {
  if [ "$#" -ne 1 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    data_dir=$1
    shift

    count=20
    while [ $count -gt 0 ]; do
      if grep 'checkpoint complete:' "${PTH_CLR_LOG_FILE}"; then
        sleep 1
        if grep 'database system is ready to accept connections' "${PTH_CLR_LOG_FILE}"; then
          return 0
        else
          return 1
        fi
      else
        echo  "  - Recovery in progress: sleeping 60 seconds for iteration ${count}"
        sleep 60
        count=$((count - 1))
      fi
    done

    if /usr/lib/postgresql/12/bin/pg_controldata -D "${PTH_CLR_DATA_BASE}" | grep -E 'Database cluster state' | grep 'in production'; then
      return 0
    else
      return 1
    fi
  fi
}

function is_postgres_open {
  if [ "$#" -ne 1 ]; then
    efatal 1 "$0: invalid number of arguments"
    return 1
  else
    data_dir=$1
    shift

    count=30
    while [ $count -gt 0 ]; do
      output=$(su - gitlab-psql -c "gitlab-psql -qtAX -p 55432 -c \"select pg_is_in_recovery();\"")
      case "$output" in
        'f')
          echo  "  + Database is open"
          return 0
          ;;
        't')
          echo  "  - Database is not open yet: sleeping 60 seconds for iteration ${count}"
          sleep 60
          count=$((count - 1))
          continue
          ;;
        *)
          efatal 1 "unable to determine is database is open (response: ${output})"
          ;;
      esac
    done

    if [ $count -eq 0 ]; then
      return 1
    else
      return 0
    fi
  fi
}

function takeoff {
  if checkpoint_postgres; then
    if create_snapshot "${ZFS_PAR_DATA_SNAP}"; then
      if create_clone "${ZFS_PAR_DATA_SNAP}" "${ZFS_CLR_DATA_BASE}" "${PTH_CLR_DATA_BASE}"; then
        if configure_clone_replica "${PTH_ZLONK_CONF_FILE}" "${PTH_CLR_DATA_BASE}"; then
          if start_postgres "${PTH_CLR_DATA_BASE}" "${PTH_CLR_CONF_FILE}"; then
            if is_postgres_ready "${PTH_CLR_DATA_BASE}"; then
              if is_postgres_open "${PTH_CLR_DATA_BASE}"; then
                /usr/lib/postgresql/12/bin/pg_controldata -D "${PTH_CLR_DATA_BASE}" | grep -E 'Database cluster state|Time of latest checkpoint'
                create_index notes updated_at
                create_index ci_builds updated_at
                echo "+ TAKEOFF COMPLETED on $(date)"
                return 0
              fi
            fi
          fi
        fi
      fi
    fi
  fi
  return 1
}

function shutdown {
  if stop_postgres "${PTH_CLR_DATA_BASE}" "${PTH_CLR_CONF_FILE}" "${PTH_CLR_LOG_FILE}"; then
    if destroy_clone "${ZFS_CLR_DATA_BASE}"; then
      if destroy_snapshot "${ZFS_PAR_DATA_SNAP}"; then
        echo "+ SHUTDOWN COMPLETED on $(date)"
        return 0
      fi
    fi
  fi
  return 1
}

# EXECUTE

echo "------------------"
echo "DEBUG: zlonk START: $(date)"
echo "DEBUG:      data.zfs: ${ZFS_PAR_DATA_BASE}"
echo "DEBUG:  snapshot.zfs: ${ZFS_PAR_DATA_SNAP}"
echo "DEBUG:     clone.zfs: ${ZFS_CLR_DATA_BASE}"
echo "DEBUG:    clone.path: ${PTH_CLR_DATA_BASE}"
echo "DEBUG: s.config.file: ${PTH_ZLONK_CONF_FILE}"
echo "DEBUG: t.config.file: ${PTH_CLR_CONF_FILE}"
echo "DEBUG:    t.log.file: ${PTH_CLR_LOG_FILE}"

if /sbin/zfs list "${ZFS_PAR_DATA_SNAP}" >/dev/null 2>&1; then
  O11Y_JOB='destroy'
  o11y_start "${O11Y_JOB}" 300
  echo "+ SHUTDOWN START on $(date)"
  if shutdown; then
    o11y_sucess "${O11Y_JOB}"
    exit 0
  else
    o11y_failed "${O11Y_JOB}"
    exit 1
  fi
else
  echo "+ TAKEOFF START on $(date)"
  O11Y_JOB='clone'
  o11y_start "${O11Y_JOB}" 2400

  takeoff_retries=1
  i=0
  while [ ${i} -le ${takeoff_retries} ]
  do
    if takeoff; then
      o11y_sucess "${O11Y_JOB}"
      exit 0
    else
      shutdown
      i=$(( i + 1 ))
    fi
  done
  o11y_failed "${O11Y_JOB}"
  exit 1
fi
