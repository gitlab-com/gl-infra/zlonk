package main

import (
	"fmt"
	"gitlab.com/gitlab-com/gl-infra/zlonk/bin/zlonkcli/action"
	"gitlab.com/gitlab-com/gl-infra/zlonk/lib/cli"
)

func main() {
	if err := action.Root.Execute(); err != nil {
		cli.ExitFatal(fmt.Sprintf("%s\n", err.Error()))
	}
}

