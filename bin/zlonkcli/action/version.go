package action

import (
	"fmt"
	"gitlab.com/gitlab-com/gl-infra/zlonk/lib/zlonk"

	"github.com/spf13/cobra"
)

func init() {
	Root.AddCommand(version)
}

var version = &cobra.Command {
	Use:   "version",
	Short: "Print zlonkcli's version number",
	Long:  `All software has versions. This is zlonkcli's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(zlonk.Version)
	},
}