package action

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/gitlab-com/gl-infra/zlonk/bin/zlonkcli/action/initialize"
	"gitlab.com/gitlab-com/gl-infra/zlonk/lib/cli"
	"gitlab.com/gitlab-com/gl-infra/zlonk/lib/zfs"
)

func init() {
	Root.AddCommand(Initialize)
}

var Initialize = &cobra.Command {
	Use:   "initialize <module> <module_options> <module_arguments>",
	Short: "Initializes Zlonk instance for module <module>",
	Long:  "Initializes Zlonk instance for module <module>",
	Run: func(cmd *cobra.Command, args []string) {
		opts, err := initialize.ParseActionInitializeArgs(args)
		if err != nil {
			cli.ExitFatal(err.Error())
		}

		z := zfs.NewDataset()
		if err = z.Find(opts.PgDataDir); err != nil {
			cli.ExitFatal(err.Error())
		}

		fmt.Println(z)
	},
}