package initialize

import (
	"errors"
)

type ActionInitializeOpts struct {
	Module 		string
	PgDataDir   string
	Instance	string
}

func ParseActionInitializeArgs (args []string) (opts *ActionInitializeOpts, err error) {
	opts = &ActionInitializeOpts{}
	switch len(args) {
	case 3:
		opts.Module = args[0]
		opts.PgDataDir = args[1]
		opts.Instance = args[2]
	default:
		return nil, errors.New("invalid number of arguments; see zlonk initialize --help")
	}
	return opts, nil
}