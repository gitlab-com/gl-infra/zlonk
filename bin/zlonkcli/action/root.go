package action

import (
	"fmt"
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var inputVerbose bool
var inputDebug bool
var inputConfig string

func init() {

	cobra.OnInitialize(initConfig)

	Root.PersistentFlags().StringVar(&inputConfig, "config", "", "zlonk configuration file")
	Root.PersistentFlags().BoolVar(&inputVerbose, "verbose", false, "Enable verbose mode")
	Root.PersistentFlags().BoolVar(&inputDebug, "debug", false, "Enable debug mode")
}

func initConfig() {
	if inputConfig == "" {
		if inputDebug == true { log.Printf("using default configuration file: $HOME/.zlonk.yaml\n") }
		viper.SetConfigType("yaml")
		viper.SetConfigName("zlonk")
		viper.AddConfigPath("/var/opt/gitlab/zlonk/etc")
	} else {
		if inputDebug == true { log.Printf("using configuration file: %s\n",inputConfig) }
		viper.SetConfigType("yaml")
		viper.SetConfigName(path.Base(inputConfig))
		viper.AddConfigPath(path.Dir(inputConfig))
	}

	if err := viper.ReadInConfig(); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(-1)
	}
}

var Root = &cobra.Command {
	Use:	"zlonkcli",
	Short:	"ZlonkCLI",
	Long:	"The zlonkcli utility dashes ZFS!",
}